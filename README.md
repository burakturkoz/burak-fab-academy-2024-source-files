# Burak Fab Academy 2024 Source Files

Hi, this is the repository for storing my source files for Fab Academy 2024. 

For my main repository please visit:

https://gitlab.fabcloud.org/academany/fabacademy/2024/labs/aalto/students/burak-turkoz

For my documentation website please visit:

https://fabacademy.org/2024/labs/aalto/students/burak-turkoz/
